<?php
try {
	$base = new PDO('mysql:host=localhost;dbname=autos', 'root', '');
	$base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch (PDOException $e){
	echo $e->getMessage();
	}

	$comando=$base->prepare("DELETE FROM auto WHERE id = :id");
	//asignar valores a parámetros
	$id= $_GET['id'];
	$comando->bindParam(':id',$id);
	//Ejecutar SQL
	if($comando->execute()){
		header('Location: index.php');
	}
?>
