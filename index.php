<?php
if(isset($_POST['search'])){
$valueToSearch=$_POST['valueToSearch'];
$query="SELECT * FROM `auto` WHERE CONCAT(`id`, `idmarca`, `nombre`, `descripcion`, `tipocombustible`, `cantidadpuertas`, `precio`) LIKE '%".$valueToSearch."%'";
$search_result=filterTable($query);
}else{
$query = "SELECT * FROM auto";
$search_result = filterTable($query);
}
function filterTable($query){
	$con=mysqli_connect("localhost","root","","autos");
	$filter_Result=mysqli_query($con, $query);
	return $filter_Result;
}
?>
<!DOCTYPE html>
<html>
<head>
	 <meta charset="utf-8">
	<title>Form - Excercise 1</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css" integrity="sha384-rtJEYb85SiYWgfpCr0jn174XgJTn4rptSOQsMroFBPQSGLdOC5IbubP6lJ35qoM9" crossorigin="anonymous">
</head>
<style type="text/css">
#search{
	margin-right: auto;
}
</style>
<body>
  <div class='container'>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Carros</h1>
             <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">


              </div>
              <a class="btn btn-outline-info " href="insertar.php">
                <i class="fas fa-plus"></i>
               Añadir Auto
              </a>
            </div>
          </div>

<form  method="post">
	<div>
		<input class="form-control" type="text" name="valueToSearch" placeholder="Buscar">
		<input type="submit" name="search" value="Buscar" class="btn btn-success" style="margin:right">
</div>
<table class="table table-bordered">
        <tr>
          <th>ID</th>
          <th>ID Marca</th>
          <th>Nombre</th>
          <th>Descripcion</th>
          <th>Tipo Combustible</th>
					<th>Cantidad de Puertas</th>
					<th>Precio</th>
					<th>Action</th>
        </tr>
        <?php while($row = mysqli_fetch_array($search_result)):?>
        <tr>
          <td><?php echo $row['id'];?></td>
					<td><?php echo $row['idmarca'];?></td>
					<td><?php echo $row['nombre'];?></td>
					<td><?php echo $row['descripcion'];?></td>
					<td><?php echo $row['tipocombustible'];?></td>
					<td><?php echo $row['cantidadpuertas'];?></td>
					<td><?php echo $row['precio'];?></td>
					<td>
						<a href="update.php?id=<?= $row['id'];?>" class="btn btn-success">Editar</a>
						<a href="delete.php?id=<?= $row['id'];?>" class="btn btn-primary">Eliminar</a>
					</td>
        </tr>
      <?php endwhile; ?>
      </table>
	</form>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
